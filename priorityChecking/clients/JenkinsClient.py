###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Command line client that interfaces to the Installer class

:author: Stefan-Gabriel CHITIC
'''
from __future__ import print_function

import logging
import optparse
import os
import sys

from priorityChecking.tools.CronCommandSender import CronCommandSender


class JenkinsClient(object):
    """ Main class for the tool """

    def __init__(self):
        """ Common setup for both clients """
        self.log = logging.getLogger(__name__)
        parser = optparse.OptionParser()
        self.parser = parser

        parser.add_option('--priority',
                          dest="priority",
                          default='10',
                          action="store",
                          help="Sets the priority")

    def main(self):
        """ Main method for the ancestor:
        call parse and run in sequence

        :returns: the return code of the call
        """
        opts, args = self.parser.parse_args()
        if len(args) > 1:
            cmd = args[0]
            arguments = [x for x in args[2:]]
            # Initializing the installer
            CronCommandSender(cmd, args=arguments,
                              priority=int(opts.priority))
        else:
            raise Exception("Argument list too short")

# Main just chooses the client and starts it
if __name__ == "__main__":
    JenkinsClient().main()
