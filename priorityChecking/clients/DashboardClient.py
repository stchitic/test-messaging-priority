from __future__ import print_function

import logging
import optparse
import json

from priorityChecking.tools.RbqClient import RbqClient


class JenkinsClient(object):
    """ Main class for the tool """

    def __init__(self):
        """ Common setup for both clients """
        self.log = logging.getLogger(__name__)
        parser = optparse.OptionParser()
        self.parser = parser

    def main(self):
        """ Main method for the ancestor:
        call parse and run in sequence

        :returns: the return code of the call
        """
        opts, args = self.parser.parse_args()
        if len(args) > 1:
            d = {
                'slot': args[0],
                'build_id': args[1],
                'platform': args[2],
                'project': args[3],
                'deployment': args[4],
            }

            RbqClient().no_priority_publish(json.dumps([d]))
        else:
            raise Exception("Argument list too short")

# Main just chooses the client and starts it
if __name__ == "__main__":
    JenkinsClient().main()
