###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Command line client that interfaces to the Installer class

:author: Stefan-Gabriel CHITIC
'''
from __future__ import print_function

import logging

from priorityChecking.tools.Scheduler import Scheduler


class SchedulerClient(object):
    def __init__(self):
        """ Common setup for both clients """
        self.log = logging.getLogger(__name__)
        self.queueName = 'commands'
        manager = Scheduler(queueName=self.queueName)
        manager.start()


# Main just chooses the client and starts it
if __name__ == "__main__":
    SchedulerClient()
