###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Command line client that interfaces to the Installer class
rp
:author: Stefan-Gabriel CHITIC
'''
from priorityChecking.tools.RbqCommon import RbqMessenger
import pika
import uuid


class RbqClient(RbqMessenger):

    def __init__(self, priority=None, *args, **kwargs):
        super(RbqClient, self).__init__(*args, **kwargs)
        self.corr_id = str(uuid.uuid4())
        if priority:
            self.priority = priority
        else:
            self.priority = 0
        self._topic_name = "topic.cvmfs_scheduler"

    def publish(self, queueName, body):
        self.channel.basic_publish(exchange=self._topic_name,
                                   routing_key=queueName,
                                   properties=pika.BasicProperties(
                                         priority=self.priority,
                                         correlation_id=self.corr_id,
                                         delivery_mode=2,
                                         ),
                                   body=body)

    def no_priority_publish(self, body):
        self.setupClient()
        props = pika.BasicProperties(delivery_mode=2)
        self.channel.basic_publish(exchange='topic.build_ready',
                                   routing_key='#',
                                   body=body,
                                   properties=props)

    def setupClient(self):
        self.connection = self._getConnection()
        self.channel = self._setupChannel(self.connection.channel())
