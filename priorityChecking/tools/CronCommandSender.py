###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Command line client that interfaces to the Installer class

:author: Stefan-Gabriel CHITIC
'''

from priorityChecking.tools.RbqClient import RbqClient
from priorityChecking.tools.RbqMessages import RbMessage


class CronCommandSender(object):
    def __init__(self, command, args=[], priority=10):
        self.client = RbqClient(priority=priority)
        self.command = command
        self.args = args
        self.client.setupClient()
        self.id = self.client.corr_id
        self.returnCode = None
        self._call()

    def _call(self):
        message = RbMessage(id=self.id, payload={
            'command': self.command,
            'args': self.args
        })
        self.client.publish('commands', message.convertToJson())

