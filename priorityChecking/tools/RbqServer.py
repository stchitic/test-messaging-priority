###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Command line client that interfaces to the Installer class

:author: Stefan-Gabriel CHITIC
'''
from priorityChecking.tools.RbqCommon import RbqMessenger
import pika
import time


class RbqServer(RbqMessenger):

    def __init__(self, caller_back, *args, **kwargs):
        super(RbqServer, self).__init__(*args, **kwargs)
        self.caller_back = caller_back
        self._topic_name = "topic.cvmfs_scheduler"
        self._timeout = 5  # Number of seconds to wait for a message
        self.last_request_time = None
        self.method = None
        self.props = None
        self.channel = None

    def start_consume(self, channel, queueName):
        all_messages = []
        if self.last_request_time is None:
            self.last_request_time = time.time()
        while True:
            time_diff = time.time() - self.last_request_time
            if len(all_messages) == 0 and time_diff > self._timeout:
                break
            while True:
                method_frame, header_frame, body = channel.basic_get(
                        queue=queueName)
                if method_frame is None:
                    break
                all_messages.append({
                    'method_frame': method_frame,
                    'header_frame': header_frame,
                    'body': body,
                    'priority': header_frame.priority
                    })
                all_messages = sorted(all_messages, key=lambda k: k['priority'])
            if len(all_messages) > 0:
                tmp = all_messages.pop()
                method_frame = tmp['method_frame']
                header_frame = tmp['header_frame']
                body = tmp['body']
                self._onRequest(channel, method_frame, header_frame, body)

    def _onRequest(self, channel, method, props, body):
        self.last_request_time = time.time()
        self.channel = channel
        self.method = method
        self.props = props
        self.caller_back(body)
        self.last_request_time = time.time()
        self.channel.basic_ack(delivery_tag=method.delivery_tag)

    def setupServer(self, queueName, bindingKeys=None):
        with self._getConnection() as self.connection:
            channel = self._setupChannel(self.connection.channel())
            channel.queue_declare(queue=queueName, durable=1)
            channel.basic_qos(prefetch_count=1)
            if bindingKeys is None:
                bindingKeys = [queueName]
            for bindingKey in bindingKeys:
                channel.queue_bind(exchange=self._topic_name,
                                   queue=queueName,
                                   routing_key=bindingKey)
            self.start_consume(channel, queueName)
