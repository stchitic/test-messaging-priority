###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Command line client that interfaces to the Installer class

:author: Stefan-Gabriel CHITIC
'''
import os
import time

from priorityChecking.tools.RbqMessages import RbMessage, DashboardMessage

from priorityChecking.tools.RbqServer import RbqServer


class Scheduler(object):
    def __init__(self, queueName='commands'):
        self.queueName = queueName
        self.server = RbqServer(self.request)

    def start(self):
        self.server.setupServer(self.queueName)

    def request(self, body):
        message = RbMessage()
        try:
            message.updateFromJson(body)
        except Exception as e:
            try:
                message = DashboardMessage.updateFromJson(body)
                if message is None:
                    return
            except Exception as e:
                raise e

        payload = message.getPayload()
        command = payload.get('command', None)
        args = payload.get('args', [])
        print("Executed: %s %s" % (command, args))
