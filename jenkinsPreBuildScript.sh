#!/bin/bash
set +x
DIRECTORY=`pwd`/virtualenv
if [ ! -d "$DIRECTORY" ]; then
	echo "Creating new enviroment"
    easy_install --user pip
    python -m pip install --user virtualenv
	python -m virtualenv $DIRECTORY
	source $DIRECTORY/bin/activate
	pip install pika
fi

source $DIRECTORY/bin/activate
python setup.py install
